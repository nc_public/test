module gitlab.com/nc_public/test

go 1.15

require (
	github.com/aws/aws-sdk-go v1.34.28
	github.com/go-kit/kit v0.10.0
	github.com/stretchr/testify v1.4.0
)
